<?php
require_once 'inc/connect.php';

// config
$fileName = 'data/origin.txt';
// endconfig
//Open the file in "reading only" mode.
$fileHandle = fopen($fileName, "r");

//If we failed to get a file handle, throw an Exception.
if ($fileHandle === false) {
    throw new Exception('Could not get file handle for: ' . $fileName);
}

//While we haven't reach the end of the file.
$i = 0;
while (!feof($fileHandle)) {
    $count = $i++;
    //Read the current line in.
    $line = fgets($fileHandle);
// Attempt insert query execution
    $sql = "INSERT INTO agent (agent_origin) VALUES ('" . trim($line) . "')";
    if (mysqli_query($link, $sql)) {
        echo "Records inserted successfully. $count" . PHP_EOL;
    } else {
        echo "ERROR: Could not able to execute $count".PHP_EOL;
    }

    //Do whatever you want to do with the line.
}

//Finally, close the file handle.
fclose($fileHandle);


// Close connection
mysqli_close($link);


